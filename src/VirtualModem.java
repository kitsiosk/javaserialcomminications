import ithakimodem.Modem;
import java.util.concurrent.TimeUnit;

import java.io.*;


public class VirtualModem {
	
	public static void main(String[] args) {
		(new VirtualModem()).demo();
	}
	
	public void demo() {
		Modem modem = new Modem();
		modem.setSpeed(1000);
		modem.setTimeout(4000);
		
		modem.open("ithaki");
		
		// Read the Welcome message ending with the word "tested."
		readPacket(modem, "", "tested.", "end");
		
		echoPacketsRequest(modem, "E8906");
		System.out.println("Phase 1 finished");
		
		modem.setSpeed(70000);
		readImage(modem, "M0814\r", "E1.jpg");
		System.out.println("Phase 2 finished");

		readImage(modem, "G3034\r", "E2.jpg");
		System.out.println("Phase 3 finished");

		gpsRequest(modem, "P5066");
		System.out.println("Phase 4 finished");
		
		modem.setSpeed(1000);
		arqPacketsRequest(modem, "Q4808", "R1141");

		System.out.println();
		System.out.println("Done!");
						
		modem.close();
	}
	
	
	public void arqPacketsRequest(Modem modem, String ackCode, String nackCode){
		long initTime = java.lang.System.currentTimeMillis();
		long endTime = java.lang.System.currentTimeMillis();
		long startTime = java.lang.System.currentTimeMillis();
		int packetDuration;
		long duration = 4*60*1000; //4 minutes in milliseconds
		boolean packetHasErrors = false;
		int attempts = 0;
		int[] attemptsArr = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		
		try{
			PrintStream out = new PrintStream("g2.txt");
			PrintStream barChart = new PrintStream("g3.txt");

			while(endTime - initTime < duration){
				if(!packetHasErrors){
					endTime = java.lang.System.currentTimeMillis();
					
					packetDuration = (int) (endTime - startTime);
					
					out.println(packetDuration);
					attemptsArr[attempts]++;
					attempts = 0;
					
					startTime = java.lang.System.currentTimeMillis();
					modem.write((ackCode + "\r").getBytes());
				}else{
					attempts++;
					modem.write((nackCode + "\r").getBytes());
				}
				
				String packet, seq, fcs;
				String[] packetArr;
				
				packet = readPacket(modem, "", "PSTOP", "end");
				packetArr = packet.split(" ");
				
				seq = packetArr[4];
				seq = seq.substring(1, seq.length()-1);
				fcs = packetArr[5];
				int fcsVal = Integer.parseInt(fcs);
				
				int next;
				int result = (int) seq.charAt(0);
				for(int i=1; i<seq.length(); ++i){
					next = (int) seq.charAt(i);
					result = result ^ next;
				}
				
				if(result == fcsVal){
					//Packet arrived with no error
					packetHasErrors = false;
				}else{
					//Packet arrived with errors
					packetHasErrors = true;
					//System.out.println("Error found");
				}
			}
			for(int i=0; i<=10; ++i){
				barChart.println(attemptsArr[i]);
			}
			out.close();
			barChart.close();
		}catch(Exception e){
			
		}
	}
	
	public void gpsRequest(Modem modem, String requestCode){
		requestCode += "T=225735403737T=225745403737T=225735403747T=225740403740\r";
		readImage(modem, requestCode, "M1.jpg");
	}
	
	public void echoPacketsRequest(Modem modem, String requestCode){
		try{
			PrintStream out = new PrintStream("g1.txt");

			long initTime = java.lang.System.currentTimeMillis();
			long endTime = java.lang.System.currentTimeMillis();
			long startTime = java.lang.System.currentTimeMillis();
			int packetDuration;
			long duration = 4*60*1000; //4 minutes in milliseconds
							
			while(endTime - initTime < duration){
				startTime = java.lang.System.currentTimeMillis();
				modem.write((requestCode + "\r").getBytes());
				readPacket(modem, "", "PSTOP", "end");
				endTime = java.lang.System.currentTimeMillis();
				packetDuration = (int) (endTime - startTime);
				out.println(packetDuration);
			}
			
			out.close();
		}catch(Exception e){
			
		}

	}
	
	// Function that reads a packet ending with the substring packetEnd
	public String readPacket(Modem modem, String packetStart, String packetEnd, String mode) {
		int k;
		String result = "";
		
		if(mode == "end"){
			while(!result.endsWith(packetEnd)){
				k = modem.read();
				result = result + (char) k;
			}
			
			return result;
		}
		
		if(mode == "start"){
			String trackStart = "";
			boolean packetStarted = false;
			
			while((k = modem.read()) != -1){
				trackStart = trackStart + (char) k;
				if(trackStart.endsWith(packetStart)){
					packetStarted = true;
					continue;
				}
				
				//10 is ASCII for \n
				if(k==10 && packetStarted){
					return result;
				}
				
				if(packetStarted){
					result = result + (char) k;
				}
			}
		}
		
		return "";
	}
	
	public void readImage(Modem modem, String requestCode, String name) {
		int k;

		try {
			modem.write(requestCode.getBytes());

			FileOutputStream out = new FileOutputStream(name);
			
			boolean image_start = false;


			while((k=modem.read()) != -1){

				if(k==255 && !image_start){
					image_start = true;
				}
				
				if(image_start){
					out.write((char) k);
				}
			}
			
			out.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
